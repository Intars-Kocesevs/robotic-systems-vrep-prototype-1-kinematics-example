Project represents part of the studies done in robotic systems mathematical modeling course in Riga Technical University
in Nov-Dec 2018. Uploaded V-REP scene offers a demo of already scripted and set-up forward kinematics for concept-model.
3D model done in FreeCAD 0.16.
V-REP version used originally - V-REP PRO EDU ver.3.5.0. (rev.4) 64bit (serialization version 21).

Script is in Lua programming language.

Hopefully, V-REP scene may be found of use for novice V-REP users.

